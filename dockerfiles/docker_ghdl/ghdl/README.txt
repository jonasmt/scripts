Passos para instalação da imagem:

1- Execute o arquivo build.sh
	Umas das formas de executar pode ser, utilizando o comando: ./build.sh
	Ou, também: bash build.sh
2- Após finalizar, execute o comando abaixo para iniciar a imagem:
	docker run -it -v ~/mydata:/opt/data ghdl:1.0 /bin/bash
