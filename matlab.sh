#!/bin/bash

VERSION=R2017a
wget -q http://upload.wikimedia.org/wikipedia/commons/2/21/Matlab_Logo.png -O /usr/share/icons/matlab.png
ln -sf /usr/local/MATLAB/$VERSION/bin/matlab /usr/local/bin/matlab
ln -sf /usr/local/MATLAB/$VERSION/bin/mbuild /usr/local/bin/mbuild
ln -sf /usr/local/MATLAB/$VERSION/bin/mcc /usr/local/bin/mcc
ln -sf /usr/local/MATLAB/$VERSION/bin/mex /usr/local/bin/mex
ln -sf /usr/local/MATLAB/$VERSION /usr/local/matlab
cat > /usr/share/applications/MATLAB_$VERSION.desktop << EOF
#!/usr/bin/env xdg-open
[Desktop Entry]
Type=Application
Icon=/usr/share/icons/matlab.png
Name=MATLAB $VERSION
Comment=Start MATLAB - The Language of Technical Computing
Exec=/usr/local/MATLAB/$VERSION/bin/matlab -desktop
Categories=Development;
EOF
