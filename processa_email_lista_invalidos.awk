#!/usr/bin/env awk

# USe: awk -f processa_email_lista_invalidos.awk lista_de_alunos_2019S2_originalJupiter_utf8.csv > lista_de_alunos_2019S2_utf8.csv
# O arquivo de entrada são as listas do jupiter em csv (UTF-8) com o cabecalho exigido pelo AMC

BEGIN { FS="," }
{
  if ($1~/^\([P|I]/){
    sub(/\(P) /,"",$1)
    sub(/\(I) /,"",$1)
    $5="pcs.sistemas.digitais+" $1 "@usp.br"
  }
  print $1 "," $2 "," $3 "," $4 "," $5
}
